using DayTwo;
using System;
using System.Collections.Generic;
using Xunit;

namespace DayTwoTest
{
    public class TestNavigation
    {
        [Fact]
        public void ProblemOne()
        {
            List<string> course = new List<string>()
            {
                "forward 5",
                "down 5",
                "forward 8",
                "up 3",
                "down 8",
                "forward 2"
            };
            const int expectedResult = 150;

            int result = Logic.Navigation(course);
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void ProblemTwo()
        {
            List<string> course = new List<string>()
            {
                "forward 5",
                "down 5",
                "forward 8",
                "up 3",
                "down 8",
                "forward 2"
            };
            const int expectedResult = 900;

            int result = Logic.CorrectedNavigation(course);
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void SubTriesToLeaveTheWaterCourse()
        {
            List<string> course = new List<string>()
            {
                "forward 5",
                "down 5",
                "forward 8",
                "down 3",
                "up 10",
                "forward 2"
            };
            const int expectedResult = 0;

            int result = Logic.Navigation(course);
            Assert.Equal(expectedResult, result);
        }
    }
}
