﻿using System;
using System.Collections.Generic;

namespace DayTwo
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Length != 2)
                {
                    throw new Exception("This program takes a two parameters 0 which algorithm to use and 1 the path for a file containing a planned course ");
                }

                int algorithm = int.Parse(args[0]);
                string[] input = System.IO.File.ReadAllLines(args[1]);
                

                List<string> course = new List<string>();
                for (int i = 0; i < input.Length; i++)
                {
                    course.Add(input[i]);
                }

                DateTime start = DateTime.Now;
                int result;
                if (algorithm == 0)
                {
                    result = Logic.Navigation(course);
                }
                else
                {
                    result = Logic.CorrectedNavigation(course);
                }
                
                DateTime end = DateTime.Now;
                Console.WriteLine(string.Format("The course distance is {0}.", result));
                Console.WriteLine(end.Subtract(start).ToString());
                Console.Write("Press any key to close: ");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred");
                Console.WriteLine(ex.Message);
                Console.Write("Press any key to close: ");
                Console.ReadKey();
            }
        }
    }

    public static class Logic
    {
        public static int Navigation(List<string> course)
        {
            int horizontalPosition = 0;
            int depth = 0;

            for (int i = 0; i < course.Count; i++)
            {
                var commands = course[i].Split(' ');
                if (commands.Length != 2)
                {
                    throw new Exception(string.Format("Corrupted course input. {0} does not match the format of 'command units'", course[i]));
                }

                if (!int.TryParse(commands[1], out int unit) || unit < 0)
                {
                    throw new Exception(string.Format("Corrupted course input. {0} is not >= 0", commands[1]));
                }

                if (string.Equals(commands[0], "forward", StringComparison.OrdinalIgnoreCase))
                {
                    horizontalPosition += unit;
                }
                else if (string.Equals(commands[0], "down", StringComparison.OrdinalIgnoreCase))
                {
                    depth += unit;
                }
                else if (string.Equals(commands[0], "up", StringComparison.OrdinalIgnoreCase))
                {
                    if (unit < depth)
                    {
                        depth -= unit;
                    }
                    else //We are a submarine, not a plane
                    {
                        depth = 0;
                    }
                }
                else
                {
                    throw new Exception("Corrupted course input. Unknown command, valid commands are forward, up, down and up.");
                }    
            }

            return horizontalPosition * depth;
        }

        public static int CorrectedNavigation(List<string> course)
        {
            int horizontalPosition = 0;
            int depth = 0;

            int aim = 0;
            for (int i = 0; i < course.Count; i++)
            {
                var commands = course[i].Split(' ');
                if (commands.Length != 2)
                {
                    throw new Exception(string.Format("Corrupted course input. {0} does not match the format of 'command units'", course[i]));
                }

                if (!int.TryParse(commands[1], out int unit) || unit < 0)
                {
                    throw new Exception(string.Format("Corrupted course input. {0} is not >= 0", commands[1]));
                }

                if (string.Equals(commands[0], "forward", StringComparison.OrdinalIgnoreCase))
                {
                    horizontalPosition += unit;
                    depth += unit * aim;
                }
                else if (string.Equals(commands[0], "down", StringComparison.OrdinalIgnoreCase))
                {
                    aim += unit;
                }
                else if (string.Equals(commands[0], "up", StringComparison.OrdinalIgnoreCase))
                {
                    aim -= unit;
                }
                else
                {
                    throw new Exception("Corrupted course input. Unknown command, valid commands are forward, up, down and up.");
                }
            }

            return horizontalPosition * depth;
        }
    }
}
